# DKX/Paginator

Paginator with factory

## Installation

```bash
$ composer require dkx/paginator
```

## Usage

```php
<?php

use DKX\Paginator\PaginatorFactory;

$itemsPerPage = 20;
$base = 1;

$factory = new PaginatorFactory();
$factory->setItemsPerPage($itemsPerPage);
$factory->setBase($base);

$totalCount = 5000;
$currentPage = 3;

$paginator = $factory->create($totalCount, $currentPage);
```

### PaginatedData

`PaginatedData` is a class which can wrap data from database and add information about pagination.

```php
<?php

use DKX\Paginator\PaginatedData;

$data = $repository->getUsers();
$paginatedData = new PaginatedData($paginator, $data);
``` 
