<?php

declare(strict_types=1);

namespace DKX\PaginatorTests;

use DKX\Paginator\PaginatorFactory;
use PHPUnit\Framework\TestCase;

final class PaginatorFactoryTest extends TestCase
{
	public function testCreate(): void
	{
		$factory = new PaginatorFactory();
		$factory->setItemsPerPage(7);
		$factory->setBase(0);

		$paginator = $factory->create(3, 1);

		self::assertSame(7, $paginator->getItemsPerPage());
		self::assertSame(0, $paginator->getBase());
	}
	public function testCreateWithItemPerPage(): void
	{
		$factory = new PaginatorFactory();
		$factory->setItemsPerPage(7);
		$factory->setBase(0);

		$paginator = $factory->create(3, 1, 9);

		self::assertSame(9, $paginator->getItemsPerPage());
		self::assertSame(0, $paginator->getBase());
	}
}
