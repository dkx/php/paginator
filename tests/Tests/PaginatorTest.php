<?php

declare(strict_types=1);

namespace DKX\PaginatorTests;

use DKX\Paginator\Paginator;
use PHPUnit\Framework\TestCase;

final class PaginatorTest extends TestCase
{
	public function testPaginator01(): void
	{
		$paginator = new Paginator(7, 3, 6, 0);

		self::assertSame(1, $paginator->getPage());
		self::assertSame(2, $paginator->getPageCount());
		self::assertSame(0, $paginator->getFirstPage());
		self::assertSame(1, $paginator->getLastPage());
		self::assertSame(6, $paginator->getOffset());
		self::assertSame(0, $paginator->getCountdownOffset());
		self::assertSame(1, $paginator->getLength());
	}

	public function testPaginator02(): void
	{
		$paginator = new Paginator(7, -1, 6, 0);

		self::assertSame(0, $paginator->getPage());
		self::assertSame(0, $paginator->getOffset());
		self::assertSame(1, $paginator->getCountdownOffset());
		self::assertSame(6, $paginator->getLength());
	}

	public function testPaginator03(): void
	{
		$paginator = new Paginator(7, -1, 7, 0);

		self::assertSame(0, $paginator->getPage());
		self::assertSame(1, $paginator->getPageCount());
		self::assertSame(0, $paginator->getFirstPage());
		self::assertSame(0, $paginator->getLastPage());
		self::assertSame(0, $paginator->getOffset());
		self::assertSame(0, $paginator->getCountdownOffset());
		self::assertSame(7, $paginator->getLength());
	}

	public function testPaginator04(): void
	{
		$paginator = new Paginator(-1, -1, 7, 0);

		self::assertSame(0, $paginator->getPage());
		self::assertSame(0, $paginator->getPageCount());
		self::assertSame(0, $paginator->getFirstPage());
		self::assertSame(0, $paginator->getLastPage());
		self::assertSame(0, $paginator->getOffset());
		self::assertSame(0, $paginator->getCountdownOffset());
		self::assertSame(0, $paginator->getLength());
	}

	public function testPaginator05(): void
	{
		$paginator = new Paginator(7, 3, 6, 1);

		self::assertSame(2, $paginator->getPage());
		self::assertSame(2, $paginator->getPageCount());
		self::assertSame(1, $paginator->getFirstPage());
		self::assertSame(2, $paginator->getLastPage());
		self::assertSame(6, $paginator->getOffset());
		self::assertSame(0, $paginator->getCountdownOffset());
		self::assertSame(1, $paginator->getLength());
	}

	public function testPaginator06(): void
	{
		$paginator = new Paginator(0, 1, 1);
		self::assertTrue($paginator->isFirst());
		self::assertTrue($paginator->isLast());

		$paginator = new Paginator(1, 1, 1);
		self::assertTrue($paginator->isFirst());
		self::assertTrue($paginator->isLast());

		$paginator = new Paginator(2, 1, 1);
		self::assertTrue($paginator->isFirst());
		self::assertFalse($paginator->isLast());

		$paginator = new Paginator(2, 2, 1);
		self::assertFalse($paginator->isFirst());
		self::assertTrue($paginator->isLast());
	}
}
