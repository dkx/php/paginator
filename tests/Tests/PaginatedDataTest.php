<?php

declare(strict_types=1);

namespace DKX\PaginatorTests;

use DKX\Paginator\PaginatedData;
use DKX\Paginator\PaginatorInterface;
use PHPUnit\Framework\TestCase;

final class PaginatedDataTest extends TestCase
{
	public function testGetters(): void
	{
		$paginator = \Mockery::mock(PaginatorInterface::class);
		$data = [];

		$paginatedData = new PaginatedData($paginator, $data);

		self::assertSame($paginator, $paginatedData->getPaginator());
		self::assertSame($data, $paginatedData->getData());
	}
}
