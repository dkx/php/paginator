<?php

declare(strict_types=1);

namespace DKX\Paginator;

final class PaginatedData implements PaginatedDataInterface
{
	/** @var PaginatorInterface */
	private $paginator;

	/** @var mixed[] */
	private $data;

	/**
	 * @param PaginatorInterface $paginator
	 * @param mixed[] $data
	 */
	public function __construct(PaginatorInterface $paginator, array $data)
	{
		$this->paginator = $paginator;
		$this->data = $data;
	}

	public function getPaginator(): PaginatorInterface
	{
		return $this->paginator;
	}

	public function getData(): array
	{
		return $this->data;
	}
}
