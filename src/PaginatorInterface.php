<?php

declare(strict_types=1);

namespace DKX\Paginator;

interface PaginatorInterface
{
	public const DEFAULT_PAGE = 1;

	public const DEFAULT_ITEMS_PER_PAGE = 10;

	public const DEFAULT_BASE = 1;

	public function getTotalCount(): int;

	public function getPage(): int;

	public function getItemsPerPage(): int;

	public function getBase(): int;

	public function getFirstPage(): int;

	public function getLastPage(): int;

	public function getPageIndex(): int;

	public function getPageCount(): int;

	public function getLength(): int;

	public function getOffset(): int;

	public function getCountdownOffset(): int;

	public function isFirst(): bool;

	public function isLast(): bool;
}
