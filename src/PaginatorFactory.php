<?php

declare(strict_types=1);

namespace DKX\Paginator;

final class PaginatorFactory implements PaginatorFactoryInterface
{
	/** @var int */
	private $itemsPerPage = PaginatorInterface::DEFAULT_ITEMS_PER_PAGE;

	/** @var int */
	private $base = PaginatorInterface::DEFAULT_BASE;

	public function setItemsPerPage(int $itemsPerPage): void
	{
		$this->itemsPerPage = $itemsPerPage;
	}

	public function setBase(int $base): void
	{
		$this->base = $base;
	}

	public function create(int $totalCount, int $page = 1, int $itemsPerPage = null): PaginatorInterface
	{
		return new Paginator($totalCount, $page, $itemsPerPage ?? $this->itemsPerPage, $this->base);
	}
}
