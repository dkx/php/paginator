<?php

declare(strict_types=1);

namespace DKX\Paginator;

interface PaginatedDataInterface
{
	public function getPaginator(): PaginatorInterface;

	public function getData(): array;
}
