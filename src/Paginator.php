<?php

declare(strict_types=1);

namespace DKX\Paginator;

final class Paginator implements PaginatorInterface
{
	/** @var int */
	private $totalCount;

	/** @var int */
	private $page;

	/** @var int */
	private $itemsPerPage;

	/** @var int */
	private $base;

	public function __construct(
		int $totalCount,
		int $page = self::DEFAULT_PAGE,
		int $itemsPerPage = self::DEFAULT_ITEMS_PER_PAGE,
		int $base = self::DEFAULT_BASE
	) {
		$this->totalCount = max(0, $totalCount);
		$this->page = $page;
		$this->itemsPerPage = max(1, $itemsPerPage);
		$this->base = $base;
	}

	public function getTotalCount(): int
	{
		return $this->totalCount;
	}

	public function getPage(): int
	{
		return $this->base + $this->getPageIndex();
	}

	public function getItemsPerPage(): int
	{
		return $this->itemsPerPage;
	}

	public function getBase(): int
	{
		return $this->base;
	}

	public function getFirstPage(): int
	{
		return $this->base;
	}

	public function getLastPage(): int
	{
		return $this->base + max(0, $this->getPageCount() - 1);
	}

	public function getPageIndex(): int
	{
		$index = max(0, $this->page - $this->base);
		return min($index, max(0, $this->getPageCount() - 1));
	}

	public function getPageCount(): int
	{
		return (int) ceil($this->totalCount / $this->itemsPerPage);
	}

	public function getLength(): int
	{
		return min($this->itemsPerPage, $this->totalCount - $this->getPageIndex() * $this->itemsPerPage);
	}

	public function getOffset(): int
	{
		return $this->getPageIndex() * $this->itemsPerPage;
	}

	public function getCountdownOffset(): int
	{
		return max(0, $this->totalCount - ($this->getPageIndex() + 1) * $this->itemsPerPage);
	}

	public function isFirst(): bool
	{
		return $this->getPageIndex() === 0;
	}

	public function isLast(): bool
	{
		return $this->getPageIndex() >= $this->getPageCount() - 1;
	}
}
