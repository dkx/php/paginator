<?php

declare(strict_types=1);

namespace DKX\Paginator;

interface PaginatorFactoryInterface
{
	public function setItemsPerPage(int $itemsPerPage): void;

	public function setBase(int $base): void;

	public function create(int $totalCount, int $page = 1, int $itemsPerPage = null): PaginatorInterface;
}
